<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="com.celestial.pandbhats.core.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <title>DSB</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="pandbUI/css/main.css" />
        <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous">
        </script>

        <script>
            $(function () {
                $("#loginFormPlaceHolder").load("pandbUI/presentation/LoginForm.html");
            });
        </script>
        <script src="pandbUI/js/main.js"></script>
    </head>
    <body>
        <%
            String dbStatus = "DB NOT CONNECTED";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();

            if (connectionStatus)
            {
                dbStatus = "Successfully connected to P&B Hats Service";
            }
        %>
        <h2><%= dbStatus%></h2>
        <%
            if (connectionStatus)
            {
        %>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item"><a class="nav-link fade in active" id="home-tab"
                                    data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                    aria-selected="true" onclick="getAllLinksAndTags()">Available Stock</a></li>
            <li class="nav-item"><a class="nav-link" id="login-tab"
                                    data-toggle="tab" href="#loginTab" role="tab" aria-controls="loginTab"
                                    aria-selected="false">Login</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade in active" id="home" role="tabpanel"
                 aria-labelledby="home-tab">
                <div class="tab-content">
                    <div id="hats-panel" class=col-sm-7>
                        <h3 id="hats-header">Tom and Jon's Hats </h3>
                        <div id="hats-list"></div>
                    </div>
                    <div class=col-sm-3>
                        <a href="#" onclick='getAllLinks()' id="tag-allCategories">Jon and Tom's Categories</a>
                        <div id="tag-list"></div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="loginTab" role="tabpanel"
                 aria-labelledby="newBookmark-tab">
                <div id="loginFormPlaceHolder" class="col-sm-10"></div>
            </div>
        </div>
        <%
            }
        %>

    </body>
</html>
